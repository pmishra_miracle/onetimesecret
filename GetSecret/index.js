const { ConnectionPool, Request } = require('mssql');
const crypto = require('crypto');

const config = {
    server: `${process.env["Server"]}`,
    database: `${process.env["Database"]}`,
    user: `${process.env["User"]}`,
    password: `${process.env["Password"]}`,
    port: 1433,
    options: {
        encrypt: true,
    },
};

async function executeQuery(query, parameters) {
  const pool = new ConnectionPool(config);

  try {
    await pool.connect();

    const request = new Request(pool);
    if (parameters) {
      parameters.forEach((param) => {
        request.input(param.name, param.type, param.value);
      });
    }

    const result = await request.query(query);
    return result.recordset;
  } finally {
    await pool.close();
  }
}

module.exports = async function (context, req) {
  const { passphrase } = req.body;
  const id = req.query.id;

  try {
      const query = `SELECT * FROM dbo.OneTimeSecrets WHERE id = '${id}'`
      const result = await executeQuery(query)

    if (result.length === 0) {
      context.res = {
        status: 404,
        body: { error: 'Data not found.' },
      };
    } else {
      const storedData = result[0];

      const hashedKeyAttempt = await new Promise((resolve, reject) => {
        crypto.pbkdf2(passphrase, storedData.salt, 100000, 64, 'sha512', (err, derivedKey) => {
          if (err) {
            reject(err);
          } else {
            resolve(derivedKey.toString('hex'));
          }
        });
      });

      if (hashedKeyAttempt === storedData.hashedKey) {
        const currentTime = new Date();
        // const storedTime = new Date(storedData.createdAt);
        const storedTime = storedData.createdAt;
        const timeDifferenceInHours = Math.abs(currentTime - storedTime) / 36e5;

        if (timeDifferenceInHours > storedData.ttl) {
            await executeQuery(`DELETE FROM dbo.OneTimeSecrets WHERE id = '${id}'`);

          context.res = {
            status: 404,
            body: { error: 'Data not found. It may have expired and was deleted.' },
          };
        } else {
          const decipher = crypto.createDecipher('aes-256-cbc', Buffer.from(hashedKeyAttempt, 'hex'));
          let decryptedMessage = decipher.update(storedData.encryptedMessage, 'hex', 'utf8') + decipher.final('utf8');

          context.res = {
            body: { secretMessage: decryptedMessage },
          };

            await executeQuery(`DELETE FROM dbo.OneTimeSecrets WHERE id = '${id}'`);

        }
      } else {
        context.res = {
          status: 401,
          body: { error: 'Unauthorized access. Enter the correct password' },
        };
      }
    }
  } catch (error) {
    console.error('Error retrieving data from the database:', error);
    context.res = {
      status: 500,
      body: { error: 'Internal Server Error' },
    };
  }
};

