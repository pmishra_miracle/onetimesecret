# oneTimeSecret

A NodeJS based Azure Function deployment to store a secret message and lock it with a passphrase. The message can then be accessed using the generated unique link and it has to be unlocked with the passphrase. The message also expires after a fixed amount of time has passed as specified by the user.

Try it yourself: [https://backend-nodejs-onetimesecret.azurewebsites.net/api/StoreSecret](https://backend-nodejs-onetimesecret.azurewebsites.net/api/StoreSecret)
