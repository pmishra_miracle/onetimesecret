const { ConnectionPool, Request } = require('mssql');
const crypto = require('crypto');

const config = {
    server: `${process.env["Server"]}`,
    database: `${process.env["Database"]}`,
    user: `${process.env["User"]}`,
    password: `${process.env["Password"]}`,
    port: 1433,
    options: {
        encrypt: true,
    },
};

async function executeQuery(query, parameters) {
  const pool = new ConnectionPool(config);
  await pool.connect();

  try {
    const request = new Request(pool);
    if (parameters) {
      parameters.forEach(param => {
        request.input(param.name, param.type, param.value);
      });
    }

    const result = await request.query(query);
    return result.recordset;
  } finally {
    await pool.close();
  }
}
module.exports = async function (context, req) {
  const { secretMessage, passphrase, ttl } = req.body;
  const salt = crypto.randomBytes(16).toString('hex');

  try {
    const hashedKey = await new Promise((resolve, reject) => {
      crypto.pbkdf2(passphrase, salt, 100000, 64, 'sha512', (err, derivedKey) => {
        if (err) {
          reject(err);
        } else {
          resolve(derivedKey.toString('hex'));
        }
      });
    });

    const cipher = crypto.createCipher('aes-256-cbc', Buffer.from(hashedKey, 'hex'));
    let encryptedMessage = cipher.update(secretMessage, 'utf8', 'hex') + cipher.final('hex');

    const idResult = await executeQuery('SELECT NEWID() AS newId');
    const id = idResult[0].newId;

    const query = `
      INSERT INTO OneTimeSecrets (id, salt, hashedKey, encryptedMessage, ttl, createdAt, updatedAt)
      VALUES ('${id}', '${salt}', '${hashedKey}', '${encryptedMessage}', ${ttl}, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
    `;

    await executeQuery(query);

    // await executeQuery(`
    //   INSERT INTO OneTimeSecrets (id, salt, hashedKey, encryptedMessage, ttl, createdAt, updatedAt)
    //   VALUES (@id, @salt, @hashedKey, @encryptedMessage, @ttl, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
    // `, [
    //   { name: 'id', type: 'UniqueIdentifier', value: id },
    //   { name: 'salt', type: 'VarChar', value: salt },
    //   { name: 'hashedKey', type: 'VarChar', value: hashedKey },
    //   { name: 'encryptedMessage', type: 'VarChar', value: encryptedMessage },
    //   { name: 'ttl', type: 'Int', value: ttl },
    // ]);

    const link = `https://backend-nodejs-onetimesecret.azurewebsites.net/api/GetSecret?id=${id}`;

    context.res = {
      body: `Secret secured. Access the secret at ${link}`,
    };

  } catch (error) {
    console.error('Error saving data to the database:', error);
    context.res = {
      status: 500,
      body: { error: 'Internal Server Error' },
    };
  }
};
