const { Sequelize, DataTypes } = require('sequelize');
// const { ConnectionPool, Request } = require('mssql');

const sequelize = new Sequelize({
  dialect: 'mssql',
  host: `${process.env["Server"]}`,
  database: `${process.env["Database"]}`,
  user: `${process.env["User"]}`,
  password: `${process.env["Password"]}`,
  port: 1433,
  dialectOptions: {
    options: {
      encrypt: true,
    },
  },
});

// const config = {
//     server: `${process.env["Server"]}`,
//     database: `${process.env["Database"]}`,
//     user: `${process.env["User"]}`,
//     password: `${process.env["Password"]}`,
//     port: 1433,
//     options: {
//         encrypt: true,
//     },
// };

const Data = sequelize.define('OneTimeSecrets', {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  salt: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  hashedKey: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  encryptedMessage: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  ttl: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.NOW,
    allowNull: false,
  },
});

module.exports = async function (context, myTimer) {
  const currentTime = new Date();

  if (myTimer.IsPastDue) {
    context.log('Timer function is running late!');
  }

  try {
    const allData = await Data.findAll();

    for (const storedData of allData) {
      const timeDifferenceInHours = Math.abs(currentTime - storedData.createdAt) / 36e5;

      if (timeDifferenceInHours > storedData.ttl) {
        await Data.destroy({
          where: { id: storedData.id },
        });

        context.log(`Data with ID ${storedData.id} has expired and was deleted.`);
      }
    }

    context.log('Timer function ran successfully.');
  } catch (error) {
    context.log.error('Error in timer function:', error);
  }
};

// async function executeQuery(query, parameters) {
//   const pool = new ConnectionPool(config);
//   await pool.connect();

//   try {
//     const request = new Request(pool);
//     if (parameters) {
//       parameters.forEach((param) => {
//         request.input(param.name, param.type, param.value);
//       });
//     }

//     const result = await request.query(query);
//     return result.recordset;
//   } finally {
//     await pool.close();
//   }
// }

// module.exports = async function (context, myTimer) {
//   const currentTime = new Date();

//   if (myTimer.IsPastDue) {
//     context.log('Timer function is running late!');
//   }

//   try {
//     const allData = await executeQuery('SELECT * FROM OneTimeSecrets');

//     for (const storedData of allData) {
//       const timeDifferenceInHours = Math.abs(currentTime - new Date(storedData.createdAt)) / 36e5;

//       if (timeDifferenceInHours > storedData.ttl) {
//         await executeQuery('DELETE FROM OneTimeSecrets WHERE id = @id', [
//           { name: 'id', type: 'UniqueIdentifier', value: storedData.id },
//         ]);

//         context.log(`Data with ID ${storedData.id} has expired and was deleted.`);
//       }
//     }

//     context.log('Timer function ran successfully.');
//   } catch (error) {
//     context.log.error('Error in timer function:', error);
//   }
// };
